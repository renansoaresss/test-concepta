-- Developer Test Concepta
Autor: Renan Soares <renansoareess@gmail.com>

Frameworks:

AngularJS (modules ui.router e ui.bootstrap)
Bootstrap 3

Outros:

HTML5/CSS3
Gulp
Node.js (modules express e body-parser)
Bower
LESS

Tecnologias: Para back-end desenvolvido conforme o documento optei por utilizar o Node.js. Durante o desenvolvimento do front-end, o Gulp foi utilizado como webserver e automatizador de tarefas (minificador, LESS, etc.), o Bower para a instalação de frameworks e módulos, além do AngularJS e Bootstrap junto ao HTML5/CSS3.

Instalação: Necessário ter o Node/NPM instalado na máquina, após a instalação, executar $ npm install na pasta da aplicação e iniciar utilizando npm start ou node server.js.

Arquivos: 

server.js & /routes/ -> Back-end
/page/ -> Front-end

A pasta page/src contém os arquivos originais, já na pasta page/dist estão os arquivos minificados e agregados usando o Gulp.

O front-end está rodando na mesma aplicação Node.js do back-end, o IP (127.0.0.1) e porta (3000) são configurados no arquivo helpers/settings.js. 