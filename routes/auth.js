var express = require('express');
var http = require('http');
var settings = require('../helpers/settings');
var router = express.Router();

router.route('/')

.post(function (req, res) {
  console.log("=== New Auth Request // E-mail: "+ req.body.email);

  var credentials = "grant_type=password&username="+req.body.email+"&password="+req.body.password;

  var options = {
    hostname: settings.concept_api,
    port: 80,
    path: settings.concept_token_url,
    method: 'POST'
  };

  var req = http.request(options, function (response) {
    response.setEncoding('utf8');
    var apiResponse = '';
    response.on('data', function (data) {
      apiResponse += data;
    });
    response.on('end', function (data) {
      apiResponseJSON = JSON.parse(apiResponse);
      if (apiResponseJSON.error) {
        res.status(401).json(apiResponseJSON.error_description);
      } else {
        res.status(200).json(apiResponseJSON);
      }
    });
  });

  req.on('error', function(error) {
     res.send(401, error.message);
  });

  req.write(credentials);
  req.end();

});



module.exports = router;
