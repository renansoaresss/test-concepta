var express = require('express');
var http = require('http');
var settings = require('../helpers/settings');
var router = express.Router();

router.route('/')

.post(function (req, res) {

  console.log("New find request // Token: " + req.body.token);

  var childrenAges = [];

  // Lembrete: fix body-parser, quando a array tem só um elemento o mesmo vira string o que causa erro na requisição a API.
  if (req.body['childrenages[]']) {
    if (req.body['childrenages[]'].length === 1) {
      childrenAges.push(req.body['childrenages[]']);
    } else {
      childrenAges = req.body['childrenages[]'];
    }
  }

  var infos = {
    Language: req.body.language,
    Currency: req.body.currency,
    destination: req.body.destination,
    DateFrom: req.body.dateFrom,
    DateTO: req.body.dateTO,
    Occupancy: {
      AdultCount: req.body.adults,
      ChildCount: req.body.children,
      ChildAges: childrenAges
    }
  }

  var options = {
    hostname: settings.concept_api,
    port: 80,
    path: settings.concept_ticketsearch_url,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': req.body.token,
    }
  };

  var req = http.request(options, function (response) {
    response.setEncoding('utf8');
    var apiResponse = '';

    response.on('data', function (data) {
      apiResponse += data;
    })
    response.on('end', function (data) {
      apiResponse = JSON.parse(apiResponse);
      res.json(apiResponse);
    });
  });

  req.on('error', function(error) {
     res.send(401, error.message);
  });

  req.write(JSON.stringify(infos));
  req.end();

});

module.exports = router;
