var express = require('express');
var fs = require('fs');
var bodyParser = require('body-parser');
var settings = require('./helpers/settings')
var auth = require('./routes/auth');
var requests = require('./routes/requests');
var path = require('path');

var app = express();

app.use(bodyParser.urlencoded({ extended: false }));

app.use(function(request, response, next) {
    response.setHeader('Access-Control-Allow-Origin', "*");
    response.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    response.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
});

// Back-end connect to Concepta API, parse and transform the data in JSON
app.use('/auth', auth);
app.use('/requests', requests);

// Front-end minified, source in the folder /page/src
app.use(express.static(path.join(__dirname, '/page/dist')));

var server = require('http').createServer(app);

server.listen(settings.server_port, settings.server_ip, function (response) {
  console.log("=== Server listening on ip " + settings.server_ip + " and port " + settings.server_port + ". ===");
});
