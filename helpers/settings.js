var config = {};

config.server_ip = "127.0.0.1";
config.server_port = 3000;
config.concept_api = "travellogix.api.test.conceptsol.com";
config.concept_token_url = "/Token";
config.concept_ticketsearch_url = "/api/Ticket/Search";

module.exports = config;
