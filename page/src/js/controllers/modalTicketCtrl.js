(function () {
  "use strict";

angular.module("test-concept").controller('modalTicketCtrl',  ['$scope', '$uibModal', '$uibModalInstance', 'dateService', 'modalTicket', modalTicketCtrl]);

function modalTicketCtrl($scope, $uibModal, $uibModalInstance, dateService, modalTicket) {
  var $modalCtrl = this;

  $modalCtrl.ticket = modalTicket;
  $modalCtrl.dateFrom = modalTicket.DateFrom.Date;
  $modalCtrl.dateTo = modalTicket.DateTo.Date;
  $modalCtrl.maxDescriptionSize = 600;
  $modalCtrl.showingFullDescription = false;
  $modalCtrl.showTicketsInfo = [];

  $modalCtrl.showFullDescription = function () {
    $modalCtrl.showingFullDescription = !$modalCtrl.showingFullDescription;
  };

  $modalCtrl.openImageModal = function (image) {
      var modalInstance = $uibModal.open({
        animation: false,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'templates/_modalImage.html',
        controller: 'modalImageCtrl',
        controllerAs: '$modalImageCtrl',
        windowTemplateUrl: 'templates/_modalImageTemplate.html',
        resolve: {
          modalImage: function () {
            return image;
          }
        }
      });
  };

  $modalCtrl.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  };

  $modalCtrl.changeTicketVisibility = function (ticketId) {
    $modalCtrl.showTicketsInfo[ticketId] = !$modalCtrl.showTicketsInfo[ticketId];
  };

  function createTicketsControlArray() {
    if ($modalCtrl.ticket.AvailableModality.length > 1) {
      $modalCtrl.showTicketsInfo = Array($modalCtrl.ticket.AvailableModality.length).fill(false);
    } else {
      $modalCtrl.showTicketsInfo.push(true);
    }
  }

  createTicketsControlArray();
  $modalCtrl.daysDifference = dateService.differenceBetweenDates($modalCtrl.dateFrom, $modalCtrl.dateTo);

}

})();
