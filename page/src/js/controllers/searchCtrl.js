(function () {
  "use strict";

angular.module("test-concept").controller("searchCtrl", ['$scope', '$state', '$filter', 'apiFindService', 'storageService', 'dateService', searchCtrl]);

function searchCtrl($scope, $state, $filter, apiFindService, storageService, dateService) {

  var $searchCtrl = this;

  $searchCtrl.search = {
    adults: "1",
    children: "0"
  };
  $searchCtrl.languages = [
    {Name: "English", Code: "ENG"},
    {Name: "Português brasileiro", Code: "PTBR"}
  ];
  $searchCtrl.currencies = [
    {Code: "USD"},
    {Code: "BRL"}
  ];

  $searchCtrl.searchError = false;
  $searchCtrl.maxAdults = 20;
  $searchCtrl.maxChildren = 10;
  $searchCtrl.maxChildrenAge = 10;
  $searchCtrl.isRequesting = false;
  $searchCtrl.search.childrenages = [];

  $searchCtrl.getArrayFromValue = function (value) {
    value = parseInt(value, 10);
    return new Array(value);
  };

  $searchCtrl.findTickets = function (search) {
    $searchCtrl.search.dateFrom = $filter('date')($searchCtrl.dateFrom, $searchCtrl.format);
    $searchCtrl.search.dateTO = $filter('date')($searchCtrl.dateTO, $searchCtrl.format);
    var daysDifference = dateService.differenceBetweenDates($filter('date')($searchCtrl.dateFrom, "yyyyMMdd"), $filter('date')($searchCtrl.dateTO, "yyyyMMdd"));
    if (daysDifference >= 1) {
      if ($searchCtrl.search.destination && $searchCtrl.search.dateFrom && $searchCtrl.search.dateTO && $searchCtrl.search.language && $searchCtrl.search.currency) {
        $searchCtrl.searchError = false;
        updateLoadingBar();
        $searchCtrl.search.token = storageService.getToken();
        apiFindRequest();
      } else {
        $searchCtrl.searchError = true;
        $searchCtrl.errorMessage = "Please make sure to fill all required fields."
      }
    } else {
      $searchCtrl.searchError = true;
      $searchCtrl.errorMessage = "Invalid date values."
    }
  };

  $searchCtrl.inlineOptions = {
    customClass: getDayClass,
    minDate: new Date(),
    showWeeks: true
  };

  $searchCtrl.dateOptionsFrom = {
    formatYear: 'yy',
    minDate: new Date(),
    startingDay: 1
  };

  $searchCtrl.dateOptionsTO = {
    formatYear: 'yy',
    minDate: new Date(),
    startingDay: 1
  }

  $searchCtrl.setDateFrom = function(year, month, day) {
    $scope.dateFrom = new Date(year, month, day);
  };

  $searchCtrl.setDateTO = function(year, month, day) {
    $scope.dateTO = new Date(year, month, day);
  };

  $searchCtrl.format = "MM/dd/yyyy"

  $searchCtrl.popupDate = {
    dateFrom: false,
    dateTO: false
  };

  $searchCtrl.openPopupDF = function() {
    $searchCtrl.popupDate.dateFrom = true;
  };

  $searchCtrl.openPopupDT = function() {
    $searchCtrl.popupDate.dateTO = true;
  };

  $searchCtrl.changeArrayChildrenAgeSize = function() {
    $searchCtrl.search.childrenages = $searchCtrl.search.childrenages.slice(0, $searchCtrl.search.children);
  };

  function setDateToday () {
    $searchCtrl.dateFrom = new Date();
    $searchCtrl.dateTO = new Date();
  };

  function getDayClass(data) {
    var date = data.date,
      mode = data.mode;
    if (mode === 'day') {
      var dayToCheck = new Date(date).setHours(0,0,0,0);

      for (var i = 0; i < $scope.events.length; i++) {
        var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

        if (dayToCheck === currentDay) {
          return $scope.events[i].status;
        }
      }
    }
    return '';
  }

  function apiFindRequest() {
    delete apiFindService.tickets;
    apiFindService.tickets = {};
    apiFindService.findTickets($searchCtrl.search)
      .success(function (tickets) {
        if (tickets.Result && tickets.Result.length > 0) {
          apiFindService.tickets = tickets;
        } else {
          $searchCtrl.searchError = true;
          $searchCtrl.errorMessage = "No ticket was found. Try again.";
        }
        $scope.$emit('newFindRequest');
        updateLoadingBar();
      })
      .error(function (error) {
        updateLoadingBar();
        $searchCtrl.searchError = true;
        $searchCtrl.errorMessage = error.error_description;
      });
  }

  function updateLoadingBar() {
    $searchCtrl.isRequesting = !$searchCtrl.isRequesting;
  }

  setDateToday();

};

})();
