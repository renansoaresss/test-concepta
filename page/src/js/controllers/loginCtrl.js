(function () {
  "use strict";

angular.module("test-concept").controller("loginCtrl", ['$scope', '$state', 'apiAuthService', 'storageService', loginCtrl]);

function loginCtrl($scope, $state, apiAuthService, storageService) {

  var $loginCtrl = this;

  $loginCtrl.user = {};
  $loginCtrl.isAuthError = false;
  $loginCtrl.isRequesting = false;

  $loginCtrl.checkCredentials = function (user) {
    if ($loginCtrl.user.email && $loginCtrl.user.password) {
      updateLoadingBar();
      apiAuthService.authUser($loginCtrl.user)
      .success(function (authInfo) {
        updateLoadingBar();
        storageService.saveToken(authInfo, $loginCtrl.user);
        $state.go('search');
      })
      .error(function (authInfo) {
        updateLoadingBar();
        $loginCtrl.isAuthError = true;
        $loginCtrl.authMessage = authInfo;
      });
    }
  }

  $loginCtrl.isAuthenticated = function() {
      $loginCtrl.user = storageService.getEmail();
      return storageService.isLogged();
  }

  $loginCtrl.logout = function() {
    storageService.destroyToken();
    $state.go('login');
  }

  function updateLoadingBar() {
    $loginCtrl.isRequesting = !$loginCtrl.isRequesting;
  }

}

})();
