(function () {
  "use strict";

angular.module("test-concept").controller("listCtrl", ['$scope', '$state', '$uibModal', 'apiFindService', 'storageService', listCtrl]);

function listCtrl($scope, $state, $uibModal, apiFindService, storageService) {

  var $listCtrl = this;

  var baseCalcPagination = 10;

  $listCtrl.quantityPerPage = 5;
  $listCtrl.totalQuantity = 0;
  $listCtrl.totalPages = 0;
  $listCtrl.actualPage = 0;
  $listCtrl.ticketsPerPageSelected = "5";

  $listCtrl.createTicketList = function (pageNumber) {
    if ($listCtrl.tickets) {
      $listCtrl.actualPage = pageNumber;
      $listCtrl.initTicketList = Number(pageNumber)*Number($listCtrl.quantityPerPage);
      $listCtrl.endTicketList  = Number($listCtrl.initTicketList)+Number($listCtrl.quantityPerPage);
      delete $listCtrl.ticketsPage;
      $listCtrl.ticketsPage = $listCtrl.tickets.slice($listCtrl.initTicketList,$listCtrl.endTicketList);
      $listCtrl.endTicketList = $listCtrl.ticketsPage.length + $listCtrl.initTicketList;
    }
  }

  $listCtrl.openTicketModal = function (ticket) {
    var modalInstance = $uibModal.open({
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'templates/_modalTicket.html',
      controller: 'modalTicketCtrl',
      controllerAs: '$modalCtrl',
      resolve: {
        modalTicket: function () {
          return ticket;
        }
      }
    });
  };

  $scope.$on('newFindRequest', function (event) { getTicketListFromService(); });

  function getTicketListFromService() {
    delete $listCtrl.totalTickets;
    delete $listCtrl.ticketsPage;
    delete $listCtrl.endTicketList;
    delete $listCtrl.initTicketList;
    delete $listCtrl.tickets;
    $listCtrl.totalPages = 0;
    if (apiFindService.tickets.Result && apiFindService.tickets.Result.length > 0) {
      $listCtrl.tickets = apiFindService.tickets.Result;
      $listCtrl.totalTickets = apiFindService.tickets.Result.length;
      $listCtrl.totalPages = Math.ceil($listCtrl.totalTickets / $listCtrl.quantityPerPage);
      $listCtrl.createTicketList(0);
      calcSelectTicketPage();
    }
  }

  function calcSelectTicketPage() {
    delete $listCtrl.ticketsPerPage;
    $listCtrl.ticketsPerPage = ["5"];
    var calcTotalTickets = $listCtrl.tickets.length;
    var actualTicketNumber = 0;

    while(calcTotalTickets != 0) {
      if (calcTotalTickets > baseCalcPagination) {
        actualTicketNumber += baseCalcPagination;
        calcTotalTickets -= baseCalcPagination;
      } else {
        actualTicketNumber += calcTotalTickets;
        calcTotalTickets = 0;
      }
      $listCtrl.ticketsPerPage.push(actualTicketNumber.toString());
    }
  }

  $listCtrl.changeQtPerPage = function() {
    $listCtrl.quantityPerPage = Number($listCtrl.ticketsPerPageSelected);
    getTicketListFromService();
  }

  $listCtrl.getArrayFromValue = function (value) {
    value = parseInt(value, 10);
    return new Array(value);
  };
}

})();
