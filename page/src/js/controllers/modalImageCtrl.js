(function () {
  "use strict";

angular.module("test-concept").controller('modalImageCtrl',  ['$scope', '$uibModalInstance', 'modalImage', modalImageCtrl]);

function modalImageCtrl($scope, $uibModalInstance, modalImage) {
  var $modalImageCtrl = this;
  $modalImageCtrl.image = modalImage.replace("small", "big");
}

})();
