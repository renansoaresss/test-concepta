(function () {
  "use strict";

angular.module("test-concept").filter("maxDescriptionSize", [maxDescriptionSize]);

function maxDescriptionSize() {
  return function (description, maxSize) {
    if (description.length > maxSize) {
      description = description.substr(0, maxSize);
      description += "...";
    }
    return description;
  }
}

})();
