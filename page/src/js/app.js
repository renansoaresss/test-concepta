(function () {
  "use strict";

angular.module("test-concept", ['ui.bootstrap', 'ui.router', 'ngStorage'])

.config(['$stateProvider', '$urlRouterProvider', '$locationProvider',
function configModule($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
    .state('app', {
      abstract: true,
      templateUrl: '/index.html'
    })
    .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: "loginCtrl as $loginCtrl"
    })
    .state('search', {
      url: '/search',
      templateUrl: 'templates/list.html',
      controller: "listCtrl as $listCtrl"
    });

    $urlRouterProvider.otherwise('/login');
}
])

.run(['$rootScope', '$state', 'storageService', runModule])
function runModule($rootScope, $state, storageService){
  $rootScope.$on('$stateChangeStart', function (event, next) {
    if (!storageService.isLogged()) {
        if (next.name !== 'login') {
          event.preventDefault();
          $state.go('login');
        }
    } else {
      if (next.name === 'login') {
        event.preventDefault();
        $state.go('search');
      }
    }
  });
};

})();
