(function () {
  "use strict";

angular.module("test-concept").service("apiFindService", ['$http', '$httpParamSerializerJQLike', 'config', apiFindService]);

function apiFindService($http, $httpParamSerializerJQLike, config) {

  var _findTickets = function (search) {
      return $http({
          url: config.baseAPI + '/requests',
          method: 'POST',
          data: $httpParamSerializerJQLike(search),
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
      });
  }

  return {
    findTickets: _findTickets
  };

};

})();
