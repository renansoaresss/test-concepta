(function () {
  "use strict";

angular.module("test-concept").factory("dateService", [dateService]);

function dateService() {

  var _differenceBetweenDates = function (stringDateFrom, stringDateTo) {
    if (stringDateFrom && stringDateTo) {
      var dateFrom = new Date(stringDateFrom.slice(0,4), stringDateFrom.slice(4,6), stringDateFrom.slice(6,8));
      var dateTo = new Date(stringDateTo.slice(0,4), stringDateTo.slice(4,6), stringDateTo.slice(6,8));
      var millisecondsDF = parseInt(dateFrom.getTime()/1000);
      var millisecondsTO = parseInt(dateTo.getTime()/1000);
      var differenceDays = ((((millisecondsTO - millisecondsDF)/60)/60)/24);
      differenceDays = differenceDays === 0 ? 1 : differenceDays;
      return differenceDays;
    }
  }

  return {
    differenceBetweenDates: _differenceBetweenDates
  };

};

})();
