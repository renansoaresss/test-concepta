(function () {
  "use strict";

angular.module("test-concept").factory("apiAuthService", ['$http', '$httpParamSerializerJQLike', 'config', apiAuthService]);

function apiAuthService($http, $httpParamSerializerJQLike, config) {

  var _authUser = function (user) {
      return $http({
          url: config.baseAPI + '/auth',
          method: 'POST',
          data: $httpParamSerializerJQLike(user),
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
      });
  }

  return {
    authUser: _authUser
  };

};

})();
