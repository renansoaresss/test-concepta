(function () {
  "use strict";

angular.module("test-concept").service("storageService", ['$localStorage', '$http', storageService]);

function storageService($localStorage, $http) {

  this.saveToken = function (authInfo, authUser) {
    $localStorage.authToken = authInfo.token_type + " " + authInfo.access_token;
    $localStorage.authUser = authUser.email;
  }

  this.getToken = function() {
    return $localStorage.authToken;
  }

  this.getEmail = function() {
    if ($localStorage.authToken) {
      return $localStorage.authUser;
    }
  }

  this.destroyToken = function() {
    delete $localStorage.authToken;
    delete $localStorage.authUser;
  }

  this.isLogged = function() {
    if ($localStorage.authToken) {
      $http.defaults.headers.common.Authorization = $localStorage.authToken;
      return true;
    } else {
      return false;
    }
  }
}

})();
